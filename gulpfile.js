var gulp = require('gulp')
  less = require('gulp-less'),
  sass = require('gulp-sass'),
  path = require('path'),
  autoprefixer = require('gulp-autoprefixer');


gulp.task('default', function () {
  return gulp.src('scss/style.scss')
    .pipe(less({
      paths: [ path.join(__dirname, 'sass', 'includes') ]
    }))
    .pipe(autoprefixer({
      browsers: ['last 25 versions'],
      cascade: false
    }))
    .pipe(gulp.dest('css'));
});

gulp.task('watch', function () {
  gulp.watch('scss/**/*.scss', ['default']);
});
